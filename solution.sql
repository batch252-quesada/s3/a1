-- login to MySQL
mysql -u root -p

-- show the list of databases
SHOW DATABASES;

-- create a new database
CREATE DATABASE blog_db;

-- switch database
USE blog_db;

-- creating tables in a db
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(50) NOT NULL,
    content VARCHAR(50) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- Create new records in users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 01:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-1-1 02:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-1-1 03:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-1-1 04:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-1-1 05:00:00");

-- Create new records in posts
INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-1-2 01:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-1-2 02:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-1-2 03:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-1-2 04:00:00");

--Retrieve records with a condition
SELECT title FROM posts WHERE user_id = 1;

SELECT email, datetime_created FROM users;

-- Updating an existing record from a table
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete a user
DELETE FROM users WHERE email = "johndoe@gmail.com";